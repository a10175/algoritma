/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */

/**
 *
 * @author Aji Andriawan
 */
public class LatihanArray {
    public static void main(String[] args){
        String hari[]={
            "Senin",
            "Selasa",
            "Rabu",
            "Kamis",
            "Jum'at",
            "Sabtu",
            "Minggu"
        };
        System.out.println("Hari "+hari[0]+" berada pada indek ke-0");     
    }
    
}
