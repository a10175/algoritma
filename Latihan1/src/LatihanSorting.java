
import java.util.Arrays;

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */

/**
 *
 * @author Aji Andriawan
 */
public class LatihanSorting {
    public static void main(String[]args){
        int[]a={4,85,7,1,0,36,-5,48};
        for (int i = 0; i < a.length - 1; i++){
            for (int j = 0; j< a.length - 1 - i; j++){
                if (a[j+1] < a[j]){
                    int temp = a[j];
                    a[j] = a[j+1];
                    a[j + 1] = temp;
                }
                System.out.print(Arrays.toString(a));
            }
        }
        
    }
 
}
