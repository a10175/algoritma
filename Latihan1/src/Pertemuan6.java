/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */

/**
 *
 * @author Aji Andriawan
 */
public class Pertemuan6 {
    public static void main(String[] args){
        int i;
        for(i=1;i<=10;i++){
            System.out.println(i);
        }
        System.out.println("#######");
        
        for(i=1;i<=10;i++){
            if(i%2==1)
            {
            System.out.println(i);
            }
        }
        System.out.println("#######");
        
        for(i=1;i<=10;i++){
            if(i%2==0)
            {
                System.out.println("Angka "+i+" adalah Genap");
            }else{
                System.out.println("Angka "+i+" adalah Ganjil");
            }
        }
    }
}